ActiveAdmin.register Number do
  index do
    selectable_column
    id_column
    column :contact
    column :number
    column :created_at
    actions
  end
end
