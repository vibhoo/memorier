ActiveAdmin.register Contact do
  index do
    selectable_column
    id_column
    column :user
    column :name
    column :created_at
    actions
  end
end
