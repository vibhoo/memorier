class SendSmsJob < ActiveJob::Base
  queue_as :default

  def perform(mobile, otp)
    content = "Hi! Thanks for signing up you're just a step away from reaching out to your loved ones. Please enter #{otp} OTP in the app to proceed."
    encoded_url = URI.encode("http://alerts.solutionsinfini.com/api/v3/index.php?method=sms&api_key=A5e22bf9ca51739a6c39ae8a87e11893e&to="+mobile+"&sender=SIDEMO&message="+content+"&format=json&custom=1,2&flash=0")
    uri = URI.parse(encoded_url)
    req = Net::HTTP.get(uri)
  end
end
