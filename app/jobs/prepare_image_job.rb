class PrepareImageJob < ActiveJob::Base
  queue_as :default
  if ENV["RAILS_ENV"] == "production" || "staging"
    url = "http://banjo.redirectme.net/"
  else
    url = "http://localhost:3000/"
  end
  @@redis = Redis.new

  def perform current_user, uploads
    images  = []
    new_images_url = []
    uploads.each do |i|
      image_url = {}
      image_url[:url]  = url + i.image.url
       images.push(image_url)
    end
    p "@@@@@@", images.to_json
    @@redis.set(current_user.authentication_token, images.to_json)
    images = @@redis.get current_user.authentication_token
    JSON.parse(images).each do |i|
      new_images_url<< i["url"]
    end
    p new_images_url
  end
end


# :url "/system/uploads/images/000/000/001/original/unnamed.jpg?1467400457"
# :url"/system/uploads/images/000/000/002/original/Promotions-Default-Image.jpg?1467714185"
# => [{:url=>"/system/uploads/images/000/000/001/original/unnamed.jpg?1467400457"}, {:url=>"/system/uploads/images/000/000/002/original/Promotions-Default-Image.jpg?1467714185"}]
