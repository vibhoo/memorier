class Device < ActiveRecord::Base
  belongs_to :user
  enum platform: { android: 0, ios: 1 }
end
