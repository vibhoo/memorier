class Upload < ActiveRecord::Base
  belongs_to :user
  #after_save :save_image
  redis = Redis.new
  has_attached_file :image,
    styles: {
      medium: '200x200^',
      thumb: '120x120^'
    },
    convert_options:  {
      thumb: "-gravity center -crop '120x120+0+0'",
      medium: "-gravity center -crop 200x200+0+0"
    },
    processors: [:thumbnail, :compression],
    default_url: "/assets/avatars/:style.png"

  validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png"]

  def self.get_image_url key
    redis = Redis.new
    r     = redis.get key
    #byebug
  end
end
