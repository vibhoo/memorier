object @attachments => :attachments

if ENV["RAILS_ENV"] == "production" || ENV["RAILS_ENV"] == "staging"
  url = "http://banjo.redirectme.net/"
else
  url = "http://localhost:3000"
end
node :attachment do |m|
  if m.status == true
    uploads = current_user.uploads.last(m.upload_images_count)
    images_url = []
    uploads.each do |i|
      images_url  <<  url + i.image.url.gsub(/\?.+/, '')
    end
    images_url
  else
    "problem uploading image"
  end
end
