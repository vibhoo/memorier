object @user => :user
attributes :name, :mobile, :date_of_birth, :country_code, :state, :city, :authentication_token
