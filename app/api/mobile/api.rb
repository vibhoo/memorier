include Geokit::Geocoders
module Mobile
  class API < Grape::API
    prefix    'api'
    format    :json
    formatter :json, Grape::Formatter::Rabl

    #rescue_from :all, :backtrace => true
    #error_formatter :json, API::ErrorFormatter
    before do
      header['Access-Control-Allow-Origin'] = '*'
      header['Access-Control-Request-Method'] = '*'
      header['Access-Control-Allow-Methods'] = 'GET, POST, PUT, PATCH, OPTIONS, DELETE'
      header['Access-Control-Allow-Headers'] = 'true'
    end

    # use Warden::Manager do |manager|
    #   manager.default_strategies :authentication_token
    #   manager.failure_app = lambda {|env| [401,{}, ["Not authorized"]]}
    # end

    helpers do

      def authenticate!
        error!("401 Unauthorized", 401) unless authenticated
      end

      def bearer_supported? bearer
        ['token'].include?(bearer)
      end

      def authenticated
        return true  if warden.authenticated?
        return false unless request.env['HTTP_AUTHORIZATION']
        authentication = request.env['HTTP_AUTHORIZATION'].split(' ')
        bearer = authentication[0]
        key    = authentication[1]
        bearer_supported?(bearer) && @user = User.find_by_authentication_token(key)
      end

      def warden
        env['warden']
      end

      def current_user
        warden.user || @user
      end

      def remote_ip
        env['REMOTE_ADDR']
      end

      def current_time
        Time.now.utc
      end

      def full_adress
        MultiGeocoder.geocode('103.255.39.201')
      end

      def logger
        API.logger
      end
    end
    mount Mobile::V1::API
    add_swagger_documentation base_path: "/mobile/",
                                api_version: 'v1',
                                hide_documentation_path: true
  end
end
