module Mobile
  module V1
    class Users < Grape::API
      version :v1
      helpers do
        def user_params
          #declared(params, include_missing: false)
          ActionController::Parameters.new(params).permit(:mobile, :password)
        end
        def user_params_sanitized
          secure_user_params = user_params
          secure_user_params
        end
      end
      resource :user do
        desc 'Register Mobile'
        params do
          requires :mobile, type: String
          optional :password, type: String, default: SecureRandom.hex(10)
        end#do
        post "/" ,rabl: 'user/create' do
          user = User.find_by_mobile(params[:mobile])
          if user
            #otp   =  rand(1000..9999)
            otp   = 2525
            #SendSmsJob.perform_later params[:mobile], otp
            @user = OpenStruct.new({status: 1, otp: otp, name: user.name, email: user.email, date_of_birth: user.date_of_birth, authentication_token: user.authentication_token, gender: user.gender, id: user.id, mobile: user.mobile})
            #error!("User Already Present", 200)
          else
            user    = User.new(user_params_sanitized.merge(current_sign_in_ip: remote_ip, current_sign_in_at: current_time))
            if user.save
              otp   = rand(1000..9999)
              @user = OpenStruct.new({status: 0, otp: otp, authentication_token: user.authentication_token, id: user.id, mobile: user.mobile})
              #SendSmsJob.perform_later params[:mobile], otp
              # User.send_otp params[:mobile], @otp
            else
              error!(@user.errors.messages, 422)
            end #unless
          end #if
        end#do
        desc "Update profile"
        params do
          requires :fname, type: String
          requires :lname, type: String
          requires :date_of_birth, type: String
          optional :email, type: String
          optional :gender, type: String
          optional :address, type: String
          optional :lat , type: String
          optional :lon, type: String
        end#do
        put "/:id", rabl: 'user/update_profile' do
          authenticate!
          current_user.update({
            :fname => params[:fname], :lname => params[:lname], email: params[:email], :date_of_birth => params[:date_of_birth], :gender => params[:gender], :address => params[:adress],:lat => params[:lat], :lon => params[:lon]
          })
          @user = current_user
        end#do
        desc "Upload Contacts"
        params do
          requires :value, type: String
        end
        post "upload-contacts", rabl: "user/upload_contacts" do
          authenticate!
          contacts = JSON.parse(params[:value])
          for i in 0..contacts.length-1
            contact = current_user.contacts.create(name: contacts[i]["key"])
            contacts[i]["value"].each do |i|
              contact.numbers.create(number: i)
            end#do
          end#for
        end#do
        desc "Upload images"
        params do
          requires :attachments, type: Array do
            requires :image, :type => Rack::Multipart::UploadedFile
            requires :session_id, type: String
            requires :timestamp, type: Integer
            requires :index, type: Integer
          end
          requires :receiver_number, type: Integer
          requires :sound_id, type: Integer
          requires :animationid, type: Integer
          #requires :images, :type => Rack::Multipart::UploadedFile
        end#do
        post "/upload-images", rabl: "user/upload_images" do
          authenticate!
          for i in 0..params[:attachments].count - 1
            image = params[:attachments][i][:image]
            index  = params[:attachments][i][:index]
            session_id = params[:attachments][i][:session_id]
            timestamp  = params[:attachments][i][:timestamp]
            upload =  Upload.create(image: ActionDispatch::Http::UploadedFile.new(image), user_id: current_user.id, index: index, session_id: session_id, timestamp: timestamp)
          end
          upload_images_count = params[:attachments].count
          @attachments = OpenStruct.new({status: true, upload_images_count: upload_images_count })
          # PrepareImageJob.perform_now current_user, current_user.uploads.last(upload_images_count)
          # attachment         = Upload.new
          # attachment.image   = ActionDispatch::Http::UploadedFile.new(images)
          # attachment.user_id = current_user.id
          # attachment.
          # attachment.save!
        end#do
        desc "Send messages"
        params do
          requires :contacts, type: String
        end#do
        post "messages" , rabl: "user/messages" do
          authenticate!
          contacts = JSON.parse(params[:contacts])
          SendMessageJob.perform_later current_user, contacts
        end#do
        desc "Mutual Friends how are already usign app."
        get "/friends", rabl: "user/friends" do
          authenticate!
          contact_numbers        = current_user.numbers.pluck("numbers.number")
          no_of_users_registered = User.where(mobile: contact_numbers)
          if no_of_users_registered.count == 0
              error!("No mutual friends right now", 200)
          else
            @no_of_users_registered = no_of_users_registered
          end
        end#do
        desc "Post commnets"
        post "/comments", rabl: "user/comments" do
          authenticate!
        end#do
      end#resource
    end#class
  end#module
end#module
