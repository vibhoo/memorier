module Mobile
  module V1
    class API < Grape::API
      mount Mobile::V1::Users
      mount Mobile::V1::Devices
    end
  end
end
