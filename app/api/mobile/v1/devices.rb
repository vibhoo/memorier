module Mobile
  module V1
    class Devices < Grape::API
      version :v1
      resource :device do
        desc 'Device API for registering device'
        params do
          requires :token, type: String
          requires :platform, type: Integer
        end
        post '/', rabl: '/device/create' do
          authenticate!
          check = Device.where(user_id: current_user.id)
          if check.present?
            token_load = Device.where(user_id: current_user.id).first
            device     = token_load.update_attributes(token: params[:token], platform: params[:platform])
            if device
              @device = device
            else
              error!("Updation Failed", 400)
            end
          else
            @device = Device.create(user_id: current_user.id, token: params[:token], platform: params[:platform])
          end
        end

        desc 'Update Device Prefrences'
        params do
          requires :enabled, type: Boolean
        end
        post '/update', rabl: 'mobile/v2/device/update' do
          authenticate!
          device_load = Device.where(user_id: current_user.id).first
          @device     = device_load.update_attribute(:enabled, params[:enabled])
        end
      end
    end
  end
end
