namespace :db do
  desc "Populate database with faker data"
  task populate: :environment do
    make_users
    make_contacts
    make_numbers
  end
end

def make_users
  puts "\n\nCREATING 50 USERS..."

  50.times do
    password = "secret"
    mobile = Faker::Number.number(10)

    User.create!(mobile: mobile,
      password: password
    )
  end
   puts "COMPLETE!\n\n"
end

def make_contacts
  puts "CREATING 100 contacts..."

  100.times do
    user = User.offset(rand(User.count)).first
    name = Faker::Name.name
    Contact.create!(name: name, user_id: user.id)
  end
  puts "COMPLETE!\n\n"
end

def make_numbers
  puts "creating 300 numbers..."

  300.times do
    contact = Contact.offset(rand(Contact.count)).first
    number  = Faker::Number.number(10)
    Number.create!(number: number, contact_id: contact.id)
  end
  puts "COMPLETE!\n\n"
end
