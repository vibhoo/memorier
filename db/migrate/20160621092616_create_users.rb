class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :password_digest
      t.string :city
      t.string :state
      t.string :country_code
      t.string :mobile,              null: false, default: ""
      t.string :authentication_token
      t.string :current_sign_in_ip
      t.date   :date_of_birth
      t.datetime :current_sign_in_at
      t.timestamps null: false
    end
    add_index :users, :mobile,                unique: true
    add_index :users,:authentication_token, unique: true
  end
end
