class AddAndRemoveColumnsFromUsers < ActiveRecord::Migration
  def change
    rename_column :users, :name, :fname
    add_column :users, :lname, :string
    add_column :users, :email,  :string
    add_column :users, :address, :text
    add_column :users, :lat, :string
    add_column :users, :lon, :string
    remove_column :users, :city
    remove_column :users, :state
    remove_column :users, :country_code
  end
end
