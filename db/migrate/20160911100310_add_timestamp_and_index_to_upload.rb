class AddTimestampAndIndexToUpload < ActiveRecord::Migration
  def change
    add_column :uploads, :session_id, :string
    add_column :uploads, :timestamp, :integer
    add_column :uploads, :index, :integer
  end
end
