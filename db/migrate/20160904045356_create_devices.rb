class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.references :user, index: true, foreign_key: true
      t.text :token
      t.integer :platform
      t.boolean :active, default: 1

      t.timestamps null: false
    end
  end
end
