class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.references :number, index: true, foreign_key: true
      t.boolean :status, default: 0

      t.timestamps null: false
    end
  end
end
