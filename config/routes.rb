require 'sidekiq/web'

Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  root 'welcome#index'

  #devise_for :users
  mount Sidekiq::Web => '/sidekiq'
  mount Mobile::API => '/mobile/'

  # constraints :subdomain => "mobile" do
  #   mount Mobile::API => '/' For live will use subdomain
  # end
end
